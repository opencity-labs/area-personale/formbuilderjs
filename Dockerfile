FROM node:14.18.2 AS build

# install simple http server for serving static content
# RUN npm install -g http-server
# EXPOSE 8080
# CMD [ "http-server", "dist" ]

ARG VUE_APP_FORMSERVER_URL=api-url-value-not-set-during-build
ARG VUE_APP_VERSION=no-version
ARG VUE_APP_FORMSERVER_READY_ONLY_URL=api-url-value-not-set-during-build
ARG CI_COMMIT_SHORT_SHA=missing-sha-arg 

# make the 'app' folder the current working directory
WORKDIR /app

# copy both 'package.json' and 'package-lock.json' (if available)
COPY package*.json ./

# install project dependencies
RUN npm install --progress=false

# copy project files and folders to the current working directory (i.e. 'app' folder)
COPY . .

# build app for production with minification
RUN npm run build

FROM caddy:2

COPY Caddyfile /etc/Caddyfile
COPY --from=build /app/dist /srv

RUN echo "formbuilder_build_info{gitCommit=\"${CI_COMMIT_SHORT_SHA}\",version=\"${VUE_APP_VERSION}\"} 1" > /srv/metrics

ENTRYPOINT [ "caddy" ]
CMD [ "run", "--config" , "/etc/Caddyfile" ]

HEALTHCHECK --interval=10s --timeout=1s \
  CMD curl -f http://localhost/health || exit 1


import { createRouter, createWebHashHistory } from 'vue-router'
import Homepage from "@/components/Homepage";
import Service from "@/components/Service";
import Services from "@/components/Services";
import BuilderPage from "@/components/BuilderPage.vue";
import NotFound from "@/components/NotFound";

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Homepage,
    meta: { excludeKeepAlive: true },
  },
  {
    path: '/builder',
    name: 'BuilderPage',
    component: BuilderPage,
    meta: { excludeKeepAlive: true },
  },
  {
    path: '/services',
    name: 'Services',
    component: Services,
    meta: { excludeKeepAlive: true },
  },
  {
    path: '/services/:id',
    name: 'Service',
    component: Service,
    meta: { excludeKeepAlive: true },
  },
  {
    path: '/:catchAll(.*)',
    name: 'Not-found',
    component: NotFound,
  },
]

const router = createRouter({
  history: createWebHashHistory(),
  routes,
})

export default router

import App from './App.vue';
import moment from "moment";
import { createApp } from 'vue'
import { configureCompat } from "@vue/compat";
import axios from 'axios'
import VueAxios from 'vue-axios'
import router from "@/router/router";
moment.locale('it');


const app = createApp(App).use(router).use(VueAxios, { $myHttp: axios })

app.config.globalProperties.$filters = {
  date(value) {
    if (value)
    return moment(String(value)).format("DD/MM/YYYY")
  },
  momentAgo(value){
    if (value)
    return moment(String(value)).fromNow()
  },
  striphtml(value) {
    if (value){
      const div = document.createElement('div');
      div.innerHTML = value;
      return div.textContent || div.innerText || "";
    }
  },
  camelcase(value) {
    if (!value) return '';
    value = value.toString();
    return value.charAt(0).toUpperCase() + value.slice(1).toLowerCase();
  },
  capitalize(value) {
    if (!value) return '';
    value = value.toString();
    return value.charAt(0).toUpperCase() + value.slice(1);
  }
}
app.provide('axios', app.config.globalProperties.axios)  // provide 'axios'

configureCompat({
  // default everything to Vue 2 behavior
  MODE: 2,
  // opt-in to Vue 3 behavior for non-compiler features
  WATCH_ARRAY: false,
  //ATTR_ENUMERATED_COERCION: false
});

app.mount('#app')

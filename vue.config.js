module.exports = {
  publicPath: process.env.NODE_ENV === 'production'
    ? ''
    : '',

  pluginOptions: {
    i18n: {
      locale: 'it',
      fallbackLocale: 'it',
      localeDir: 'locales',
      enableInSFC: true,
    },
  },
  chainWebpack: (config) => {
    config.resolve.alias.set('vue', '@vue/compat')
    config.module.rules.delete('eslint');
    config.module
      .rule('vue')
      .use('vue-loader')
      .tap((options) => {
        return {
          ...options,
          compilerOptions: {
            compatConfig: {
              MODE: 2,
              WATCH_ARRAY: false
            }
          }
        }
      })
  }
};
